#ifndef F_CPU
#define F_CPU 1000000UL
#endif

#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/io.h>
#include "twi.h"
#include <math.h>
#include <stdlib.h>

#define LED_PORT        PORTB
#define LED0            PB0
#define LED1            PB1
#define LED_DDR_CONF    DDRB |= (1<<DDB0) | (1<<DDB1)

#define LED0_OFF        LED_PORT |= (1<<LED0)
#define LED0_ON         LED_PORT &= ~(1<<LED0)

#define LED1_OFF        LED_PORT |= (1<<LED1)
#define LED1_ON         LED_PORT &= ~(1<<LED1)

volatile uint32_t timer_overflow_counter = 0;

char* toString(uint32_t num)
{
    char *str = malloc(10*sizeof(char));
    uint8_t i;
    for( i=0; i<10; i++)
    {
        uint32_t divisor = (pow(10,(10-1-i)));
        uint8_t n = num/divisor;
        num %= divisor;
        str[i] = n + '0';
    }
    return str;
}

void init_leds()
{
    LED_DDR_CONF;
    LED0_OFF;
    LED1_OFF;
}

void init_timer()
{
    TIMSK = 0x04;
    TCCR1A = 0x00;
    TCCR1B = 0x03;
    TIFR |= (1<<TOV1);
}

void power_down()
{
    sei();
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
}

ISR(TIMER1_OVF_vect)
{
    uint32_t tmp_timer_overflow_counter;
    tmp_timer_overflow_counter = timer_overflow_counter;

    tmp_timer_overflow_counter++;
//    twi_write_data(0x44, (uint8_t*)toString(tmp_timer_overflow_counter),10);

    timer_overflow_counter = tmp_timer_overflow_counter;
}

int main()
{
//    twi_init(1,255);
    init_leds();
    init_timer();

    while(1)
    {
        while(timer_overflow_counter<5)
        {
            power_down();
    //        twi_init(1,255);
    //        twi_write_data(0x44,(uint8_t*)toString(tifr),10);
            LED0_ON;
            _delay_ms(200);
            LED0_OFF;
        }
        timer_overflow_counter = 0;
        LED1_ON;
        _delay_ms(1000);
        LED1_OFF;
    }
}

