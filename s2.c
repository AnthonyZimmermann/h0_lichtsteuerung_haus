#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>

#define ADC_RAND_PORT                           PORTC
#define ADC_RAND_PIN                            PC1
#define ADC_RAND_DDR_CONF                       DDRC |= (1<<DDC1)
#define ADC_RAND_PULLUP_CONF                    ADC_RAND_PORT &= ~(1<<ADC_RAND_PIN)

/*
 * soure:                           PC1         0x01
 * result alignment:                right       (0<<ADLAR)
 * Vref:                            VCC/2       (1<<REFS0) | (1<<REFS1)
 */
#define ADC_RAND_ADMUX_CONF                     ADMUX = TODO
/*
 * prescaler:                       8           0x03
 */
#define ADC_RAND_ADCSRA_CONF                    ADCSRA = TODO

#define ADC_RAND_ENABLE                         ADCSRA |= (1<<ADEN)
#define ADC_RAND_DISABLE                        ADCSRA &= ~(1<<ADEN)
#define ADC_RAND_START                          ADCSRA |= (1<<ADSC)
#define ADC_RAND_ACTIVE                         ADCSRA & (1<<ADSC)

typedef struct Person Person;
typedef struct Room Room;
typedef struct Edge Edge;

struct Person {
    Room* current_room;
    Room* visit_room;
};

struct Edge {
    Room* room; // unidirectional edge
    uint16_t change_factor;
    uint16_t visit_factor;
    uint8_t activated;
};

struct Room {
    uint8_t state_bit;
    uint8_t num_edges;
    Edge** edges;
};

void set_state(uint32_t state)
{
    printf("state: %x", state);
}

Edge* random_edge(Room* room)
{

}

void init_adc_rand()
{
    ADC_RAND_DDR_CONF;
    ADC_RAND_PULLUP_CONF;
    ADC_RAND_ADMUX_CONF;
    ADC_RAND_ADCSRA_CONF;
}

uint32_t random_number32()
{
    uint32_t random_number;
    ADC_RAND_ENABLE;
    uint8_t i;
    for(i=0; i<8; i++)
    {
        ADC_RAND_START;
        while(ADC_RAND_ACTIVE){}
        random_number = (random_number<<4) | (ADCW&0xF)
    }
    ADC_RAND_DISABLE;
    return random_number;
}

int main()
{

}
