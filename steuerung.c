#ifndef F_CPU
#define F_CPU 1000000UL
#endif

#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <stdlib.h>

// all ports as output ports
#define DDRB_CONF           DDRB = 0xff
#define DDRC_CONF           DDRC = 0xff
#define DDRD_CONF           DDRD = 0xff

// make all LEDs on -> set pins high and dont drive leds to ground
#define PORTB_HI            PORTB = 0xff
#define PORTC_HI            PORTC = 0xff
#define PORTD_HI            PORTD = 0xff

// 0b.DDDD.DDDD.CCCC.CCCC.BBBB.BBBB
// define LEDS
# define B_OFFSET           0
# define C_OFFSET           8
# define D_OFFSET           16
# define B_SHIFTED          ((uint32_t)1<<B_OFFSET)
# define C_SHIFTED          ((uint32_t)1<<C_OFFSET)
# define D_SHIFTED          ((uint32_t)1<<D_OFFSET)
// LED01
# define LED02              (D_SHIFTED<<0)
# define LED03              (D_SHIFTED<<1)
# define LED04              (D_SHIFTED<<2)
# define LED05              (D_SHIFTED<<3)
# define LED06              (D_SHIFTED<<4)
// LED07
// LED08
# define LED09              (B_SHIFTED<<6)
# define LED10              (B_SHIFTED<<7)
# define LED11              (D_SHIFTED<<5)
# define LED12              (D_SHIFTED<<6)
# define LED13              (D_SHIFTED<<7)
# define LED14              (B_SHIFTED<<0)
# define LED15              (B_SHIFTED<<1)
# define LED16              (B_SHIFTED<<2)
# define LED17              (B_SHIFTED<<3)
# define LED18              (B_SHIFTED<<4)
# define LED19              (B_SHIFTED<<5)
// LED20
// LED21
// LED22
# define LED23              (C_SHIFTED<<0)
# define LED24              (C_SHIFTED<<1)
# define LED25              (C_SHIFTED<<2)
# define LED26              (C_SHIFTED<<3)
# define LED27              (C_SHIFTED<<4)
# define LED28              (C_SHIFTED<<5)

#define EDGE_CHANGE         0
#define EDGE_VISIT          1
#define PERSON_RESTING      0
#define PERSON_VISITING     1

#define STATE_TIME_S        5
#define NIGHT_TIME_S        120
#define DAY_TIME_S          10

typedef struct Person Person;
typedef struct Room Room;
typedef struct Edge Edge;

struct Person {
    Room* current_room;
    Room* visit_room;
    uint32_t state; // state of leds with respect to the specific person
    uint8_t action_state;
};

struct Edge {
    Room* room; // room to which that edge points
    uint16_t change_factor;
    uint16_t visit_factor;
    uint8_t activated;
};

struct Room {
    uint8_t state_bit;
    uint8_t num_edges;
    Edge** edges;
};

volatile uint32_t timer_overflow_counter;
uint8_t daytime = 1;
uint32_t current_state_id = 0;
uint8_t night_time_remaining_state_locks = 0;

void all_leds_off()
{
    PORTB_HI;
    PORTC_HI;
    PORTD_HI;
}

void init_leds()
{
    DDRB_CONF;
    DDRC_CONF;
    DDRD_CONF;

    all_leds_off();
}

void init_timer()
{
    // init timer/counter 1
    TIMSK = 0x04; // set TOIE
    TCCR1A = 0x00; // COM1A off, COM1B off, no FOC, WGM=xx00
    TCCR1B = 0x02; // ICNC1 off, ICES1 falling, WGM=00xx CS=010 -> prescaler 8 ~ 0.5s overflow
    TIFR |= (1<<TOV1); // clear TOV1 flag if set
}

void init()
{
    cli();
    init_leds();
    init_timer();
    sei();
}

void enter_idle_mode()
{
    sei();
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
}

void set_state(uint32_t state)
{
    // get states for individual ports
    uint8_t portb_state = ~(((uint32_t)(state)>>B_OFFSET) & (0xff));
    uint8_t portc_state = ~(((uint32_t)(state)>>C_OFFSET) & (0xff));
    uint8_t portd_state = ~(((uint32_t)(state)>>D_OFFSET) & (0xff));

    // turn leds on
    PORTB |= portb_state;
    PORTC |= portc_state;
    PORTD |= portd_state;

    _delay_ms(200);

    // turn leds off
    PORTB &= portb_state;
    PORTC &= portc_state;
    PORTD &= portd_state;
}


ISR(TIMER1_OVF_vect)
{
    timer_overflow_counter++;
}

void lock_state()
{
    // determine how long the current state is locked
    uint32_t overflow_counter_compare_value;
    if (daytime)
    {
        overflow_counter_compare_value = DAY_TIME_S;
        night_time_remaining_state_locks = NIGHT_TIME_S / STATE_TIME_S;
        all_leds_off();
        daytime = 0;
    }
    else
    {
        night_time_remaining_state_locks -= 1;
        if (night_time_remaining_state_locks == 0)
        {
            daytime = 1;
        }
        overflow_counter_compare_value = STATE_TIME_S;
    }

    timer_overflow_counter = 0;
    overflow_counter_compare_value *= 2; // one overflow every 0.5 seconds
    while (timer_overflow_counter < overflow_counter_compare_value)
    {
        enter_idle_mode();
    }
}

void update_state(uint8_t num_persons, Person* persons)
{
    uint32_t total_state = 0;
    uint8_t i;
    for (i=0; i<num_persons; i++)
        total_state |= persons[i].state;

    set_state(total_state);
}

Edge* random_edge(Room* room)
{
    uint32_t weights_cumulative_sum = 0;
    uint32_t total_weights = 0;
    uint8_t i;
    for (i=0; i<room->num_edges; i++)
    {
        total_weights += room->edges[i]->change_factor;
        total_weights += room->edges[i]->visit_factor;
    }

    uint32_t r = rand()%total_weights;

    for (i=0; i<room->num_edges; i++)
    {
        weights_cumulative_sum += room->edges[i]->change_factor;
        if (r <= weights_cumulative_sum)
            room->edges[i]->activated = EDGE_CHANGE;
            return room->edges[i];
        weights_cumulative_sum += room->edges[i]->visit_factor;
        if (r <= weights_cumulative_sum)
            room->edges[i]->activated = EDGE_VISIT;
            return room->edges[i];
    }
    return room->edges[0]; // fallback should never be reached
}

void go_to_room(Person* person, Room* room)
{
    person->state = (1<<room->state_bit);
    person->current_room = room;
}

void visit_room(Person* person, Room* room)
{
    person->state |= (1<<room->state_bit);
    person->visit_room = room;
    person->action_state = PERSON_VISITING;
}

void visit_room_return(Person* person)
{
    person->state &= ~(1<<person->visit_room->state_bit);
    person->action_state = PERSON_RESTING;
}

void stay_room(Person* person) {}

void next_action(Person* person)
{
    if (person->action_state == PERSON_VISITING)
    {
        Edge* e = random_edge(person->current_room);
        if (e->room != person->visit_room)
            visit_room_return(person);
    }
    else
    {
        Edge* e = random_edge(person->current_room);
        if (e->room != person->current_room)
        {
            if (e->activated == EDGE_CHANGE)
                go_to_room(person, e->room);
            else
                visit_room(person, e->room);
        }
        else // stay
        {
            stay_room(person);
        }
    }
}

int main()
{
    init();

    Room all_rooms[3];
    uint8_t num_rooms = sizeof(all_rooms)/sizeof(all_rooms[0]);

    Room livingroom;
    Room bathroom;
    Room kitchen;

    livingroom.state_bit = 0;
    bathroom.state_bit = 1;
    kitchen.state_bit = 2;

    all_rooms[0] = livingroom;
    all_rooms[1] = bathroom;
    all_rooms[2] = kitchen;

    livingroom.num_edges = 3;
    livingroom.edges = (Edge**)malloc(sizeof(Edge*)*livingroom.num_edges);
    Edge livingroom0 = { &livingroom, 800, 0, EDGE_CHANGE };
    Edge livingroom1 = { &bathroom, 10, 90, EDGE_CHANGE };
    Edge livingroom2 = { &kitchen, 70, 30, EDGE_CHANGE };
    livingroom.edges[0] = &livingroom0;
    livingroom.edges[1] = &livingroom1;
    livingroom.edges[2] = &livingroom2;

    kitchen.num_edges = 3;
    kitchen.edges = (Edge**)malloc(sizeof(Edge*)*kitchen.num_edges);
    Edge kitchen0 = { &kitchen, 700, 0, EDGE_CHANGE };
    Edge kitchen1 = { &bathroom, 10, 90, EDGE_CHANGE };
    Edge kitchen2 = { &livingroom, 60, 40, EDGE_CHANGE };
    kitchen.edges[0] = &kitchen0;
    kitchen.edges[1] = &kitchen1;
    kitchen.edges[2] = &kitchen2;

    bathroom.num_edges = 3;
    bathroom.edges = (Edge**)malloc(sizeof(Edge*)*bathroom.num_edges);
    Edge bathroom0 = { &bathroom, 200, 0, EDGE_CHANGE };
    Edge bathroom2 = { &livingroom, 300, 100, EDGE_CHANGE  };
    Edge bathroom1 = { &kitchen, 400, 0, EDGE_CHANGE };
    bathroom.edges[0] = &bathroom0;
    bathroom.edges[1] = &bathroom1;
    bathroom.edges[2] = &bathroom2;

    Person persons[2];
    uint8_t num_persons = sizeof(persons)/sizeof(persons[0]);
    uint8_t i;
    for (i=0; i<num_persons; i++)
        persons[i] = (Person){ &all_rooms[rand()%num_rooms], &all_rooms[rand()%num_rooms], 0, PERSON_RESTING };

    update_state(num_persons, persons);

    while (1)
    {
//        for (i=0; i<num_persons; i++)
//            next_action(&persons[i]);
//        update_state(num_persons, persons);
//        lock_state();
    }
}
