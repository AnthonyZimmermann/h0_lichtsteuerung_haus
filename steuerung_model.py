import random
import time

slottime = 0.1

class Person(object):
    def __init__(self, name):
        self.name = name
        self.current_room = None
        self.visit_room = None
        self.resting_factor = 1
        self.state = 0
        initEdges = []
        initEdges.append(Edge(livingroom, 10, 0))
        initEdges.append(Edge(bathroom, 50, 0))
        initEdges.append(Edge(kitchen, 40, 0))

        e = random_edge(initEdges)

        go_to_room(self, e.room)

class Edge(object):
    def __init__(self, room, change_factor, visit_factor):
        self.room = room
        self.change_factor = change_factor
        self.visit_factor = visit_factor
        self.activated = ""

class Room(object):
    def __init__(self, bit, name):
        self.name = name
        self.bit = bit
        self.edges = []

def random_edge(edges):
    weights_cumulative_sum = 0
    total_weights = 0
    for e in edges:
        total_weights += e.change_factor
        total_weights += e.visit_factor

    r = random.random()*total_weights

    for e in edges:
        weights_cumulative_sum += e.change_factor
        if r <= weights_cumulative_sum:
            e.activated = "change"
            return e
        weights_cumulative_sum += e.visit_factor
        if r <= weights_cumulative_sum:
            e.activated = "visit"
            return e

def print_state(persons):
    total_state = 0
    for p in persons:
        total_state |= p.state

    for i in range(32):
        print((total_state & 0b1), end=".")
        total_state >>= 1
    print("")

def slot_sleep():
    time.sleep(slottime)

def next_action(person):
    # currently visiting another room?
    # on visit
    if person.visit_room is not None:
        e = random_edge(person.visit_room.edges)
        if e.room != person.visit_room.edges:
            visit_room_return(person)
    # was resting in the current room
    else:
        e = random_edge(person.current_room.edges)
        # dont stay 
        if e.room != person.current_room:
            if e.activated == "change":
                go_to_room(person, e.room)
            else: # e.activated == "visit"
                visit_room(person, e.room)
        # stay
        else:
            stay_room(person)

def go_to_room(person, room):
    print("{} goes to {}".format(person.name, room.name))
    person.state = (1<<room.bit)
    old_room = person.current_room
    person.current_room = room

def visit_room(person, room):
    print("{} visits {}".format(person.name, room.name))
    person.state |= (1<<room.bit)
    person.visit_room = room

def visit_room_return(person):
    print("{} returns to {}".format(person.name, person.current_room.name))
    person.state &= ~(1<<person.visit_room.bit)
    person.visit_room = None

def stay_room(person):
    print("{} stays at {}".format(person.name, person.current_room.name))
    person.resting_factor = 1

if __name__ == "__main__":

    livingroom  = Room(0, "livingroom")
    bathroom    = Room(1, "bath")
    kitchen     = Room(2, "kitchen")

    livingroom.edges.append(Edge(livingroom, 800, 0))
    livingroom.edges.append(Edge(bathroom, 10, 90))
    livingroom.edges.append(Edge(kitchen, 70, 30))

    kitchen.edges.append(Edge(kitchen, 700, 0))
    kitchen.edges.append(Edge(bathroom, 10, 90))
    kitchen.edges.append(Edge(livingroom, 60, 140))

    bathroom.edges.append(Edge(bathroom, 200, 0))
    bathroom.edges.append(Edge(livingroom, 300, 100))
    bathroom.edges.append(Edge(kitchen, 400, 0))

    persons = []
    print("people come home")
    persons.append(Person("anthony"))
    persons.append(Person("linda"))

    print_state(persons)
    slot_sleep()
    print("...")

    while True:
        for p in persons:
            next_action(p)
        print_state(persons)
        print("...")
        slot_sleep()
