#define EDGE_CHANGE         100
#define EDGE_VISIT          101
#define PERSON_RESTING      110
#define PERSON_VISITING     111

#define STATE_TIME_S        5
#define NIGHT_TIME_S        20
//#define DAY_TIME_S          10

#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>

typedef struct Person Person;
typedef struct Room Room;
typedef struct Edge Edge;

struct Person {
    Room* current_room;
    Room* visit_room;
//    uint32_t state; // state of leds with respect to the specific person
//    uint8_t action_state;
};

struct Edge {
    Room* room; // room to which that edge points
    uint16_t change_factor;
    uint16_t visit_factor;
    uint8_t activated;
};

struct Room {
    uint8_t state_bit;
    uint8_t num_edges;
    Edge** edges;
};

//uint8_t daytime = 1;
//uint8_t night_time_remaining_state_locks = 0;
uint8_t remaining_state_locks = 0;

void set_state(uint32_t state)
{
    printf("state: %x", state);
}

void init_night_time()
{
    remaining_state_locks = NIGHT_TIME_S / STATE_TIME_S;
}

void lock_state()
{
    // determine how long the current state is locked
    uint32_t overflow_counter_compare_value;
    overflow_counter_compare_value = STATE_TIME_S;

    sleep(overflow_counter_compare_value);
}

void update_state(uint8_t num_persons, Person* persons)
{
    uint32_t total_state = 0;
    uint8_t i;
    for (i=0; i<num_persons; i++)
    {
        total_state |= persons[i].state;
    }

    set_state(total_state);
}

Edge* random_edge(Room* room)
{
    uint32_t weights_cumulative_sum = 0;
    uint32_t total_weights = 0;
    uint8_t i;
    for (i=0; i<room->num_edges; i++)
    {
        total_weights += room->edges[i]->change_factor;
        total_weights += room->edges[i]->visit_factor;
    }

    uint32_t r = random()%total_weights;

    for (i=0; i<room->num_edges; i++)
    {
        weights_cumulative_sum += room->edges[i]->change_factor;
        if (r <= weights_cumulative_sum)
        {
            room->edges[i]->activated = EDGE_CHANGE;
            return room->edges[i];
        }
        weights_cumulative_sum += room->edges[i]->visit_factor;
        if (r <= weights_cumulative_sum)
        {
            room->edges[i]->activated = EDGE_VISIT;
            return room->edges[i];
        }
    }
    return room->edges[0]; // fallback should never be reached
}

void go_to_room(Person* person, Room* room)
{
    person->current_room = room;
}

void visit_room(Person* person, Room* room)
{
    person->visit_room = room;
    person->action_state = PERSON_VISITING;
}

void visit_room_return(Person* person)
{
    person->visit_room = NULL;
    person->action_state = PERSON_RESTING;
}

void stay_room(Person* person) {}

void next_action(Person* person)
{
    if (person->action_state == PERSON_VISITING)
    {
        Edge* e = random_edge(person->current_room);
        if (e->room != person->visit_room)
            visit_room_return(person);
    }
    else
    {
        Edge* e = random_edge(person->current_room);
        if (e->room != person->current_room)
        {
            if (e->activated == EDGE_CHANGE)
                go_to_room(person, e->room);
            else
                visit_room(person, e->room);
        }
        else // stay
        {
            stay_room(person);
        }
    }
}

int main()
{
    int r = rand()%325;
    printf("random number: %d", r);
    fflush(stdout);
    Room all_rooms[3];
    uint8_t num_rooms = sizeof(all_rooms)/sizeof(all_rooms[0]);

    Room livingroom;
    Room bathroom;
    Room kitchen;

    livingroom.state_bit = 0;
    bathroom.state_bit = 1;
    kitchen.state_bit = 2;

    all_rooms[0] = livingroom;
    all_rooms[1] = bathroom;
    all_rooms[2] = kitchen;

    livingroom.num_edges = 3;
    livingroom.edges = (Edge**)malloc(sizeof(Edge*)*livingroom.num_edges);
    Edge livingroom0 = { &livingroom, 800, 0, EDGE_CHANGE };
    Edge livingroom1 = { &bathroom, 10, 90, EDGE_CHANGE };
    Edge livingroom2 = { &kitchen, 70, 30, EDGE_CHANGE };
    livingroom.edges[0] = &livingroom0;
    livingroom.edges[1] = &livingroom1;
    livingroom.edges[2] = &livingroom2;

    kitchen.num_edges = 3;
    kitchen.edges = (Edge**)malloc(sizeof(Edge*)*kitchen.num_edges);
    Edge kitchen0 = { &kitchen, 700, 0, EDGE_CHANGE };
    Edge kitchen1 = { &bathroom, 10, 90, EDGE_CHANGE };
    Edge kitchen2 = { &livingroom, 60, 40, EDGE_CHANGE };
    kitchen.edges[0] = &kitchen0;
    kitchen.edges[1] = &kitchen1;
    kitchen.edges[2] = &kitchen2;

    bathroom.num_edges = 3;
    bathroom.edges = (Edge**)malloc(sizeof(Edge*)*bathroom.num_edges);
    Edge bathroom0 = { &bathroom, 200, 0, EDGE_CHANGE };
    Edge bathroom2 = { &livingroom, 300, 100, EDGE_CHANGE  };
    Edge bathroom1 = { &kitchen, 400, 0, EDGE_CHANGE };
    bathroom.edges[0] = &bathroom0;
    bathroom.edges[1] = &bathroom1;
    bathroom.edges[2] = &bathroom2;

    Person persons[2];
    uint8_t num_persons = sizeof(persons)/sizeof(persons[0]);
    uint8_t i;
    for (i=0; i<num_persons; i++)
        persons[i] = (Person){ &all_rooms[random()%num_rooms], &all_rooms[random()%num_rooms], 0, PERSON_RESTING };

    update_state(num_persons, persons);

    while (1)
    {
//        for (i=0; i<num_persons; i++)
//            next_action(&persons[i]);
//        update_state(num_persons, persons);
//        lock_state();
    }
}
