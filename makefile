CC=gcc
CC_FLAGS=-Wall -g -Os

SRC_FILES_MODEL=steuerung_model.c

AVR_CC=avr-gcc
AVR_CC_FLAGS=-Wall -g -Os -mmcu=atmega8

COPY=avr-objcopy
COPY_FLAGS=-j .text -j .data -O ihex

SRC_FILES=steuerung.c
OUT_FILE=a.out
HEX_FILE=a.hex

FLASHER=avrdude
PROGRAMMER=raspberrypi_2
FLASHER_FLAGS=-p atmega8 -c $(PROGRAMMER) -v

all: $(SRC_FILES)
	$(AVR_CC) $(AVR_CC_FLAGS) $(SRC_FILES)
	$(COPY) $(COPY_FLAGS) $(OUT_FILE) $(HEX_FILE)

flash: $(SRC_FILES)
	$(AVR_CC) $(AVR_CC_FLAGS) $(SRC_FILES)
	$(COPY) $(COPY_FLAGS) $(OUT_FILE) $(HEX_FILE)
	$(FLASHER) $(FLASHER_FLAGS) -U flash:w:$(HEX_FILE):i

model: $(SRC_FILES)
	$(CC) $(CC_FLAGS) $(SRC_FILES_MODEL) -o model_c
